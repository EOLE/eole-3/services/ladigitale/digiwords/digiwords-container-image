FROM hub.eole.education/proxyhub/alpine:latest as BUILD
RUN apk add --no-cache git npm
COPY app-version /tmp/
RUN cd /tmp && \
    git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/ladigitale/digiwords/digiwords-sources.git && \
    cd digiwords-sources && \
    git checkout $(cat ../app-version) && \
    cp -a /tmp/digiwords-sources /tmp/src
WORKDIR /tmp/src
RUN npm install && \
    npm run build
FROM hub.eole.education/proxyhub/alpine:latest
RUN apk add --no-cache apache2 php83-apache2 php83-zip php83-pdo_pgsql php83-pdo_sqlite php83-pecl-redis
COPY --from=BUILD /tmp/src/dist/ /var/www/localhost/htdocs/
COPY 000-default.conf /etc/apache2/conf.d/000-default.conf
RUN sed -i "s/^ServerTokens .*$/ServerTokens Prod/" /etc/apache2/httpd.conf
RUN sed -i "s/^ServerSignature .*$/ServerSignature Off/" /etc/apache2/httpd.conf
RUN chown -R apache: /var/www/localhost/htdocs
EXPOSE "80"
CMD ["/usr/sbin/httpd", "-D", "FOREGROUND", "-f", "/etc/apache2/httpd.conf"]
